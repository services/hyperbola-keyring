V=20201208

PREFIX = /usr/local

install:
	install -dm755 $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	install -dm755 $(DESTDIR)$(PREFIX)/share/licenses/hyperbola-keyring/
	install -m0644 hyperbola{.gpg,-trusted,-revoked} $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	install -m0644 legalcode.txt $(DESTDIR)$(PREFIX)/share/licenses/hyperbola-keyring/

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/share/pacman/keyrings/hyperbola{.gpg,-trusted,-revoked}
	rm -f $(DESTDIR)$(PREFIX)/share/licenses/hyperbola-keyring/legalcode.txt
	rmdir -p --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	rmdir -p --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/share/licenses/hyperbola-keyring/

dist:
	mkdir -pv hyperbola-keyring-$(V)/
	cp -v Makefile hyperbola.gpg hyperbola-revoked hyperbola-trusted legalcode.txt hyperbola-keyring-$(V)/
	bsdtar capf hyperbola-keyring-$(V).tar.lz hyperbola-keyring-$(V)/
	gpg --detach-sign --use-agent hyperbola-keyring-$(V).tar.lz
	rm -rv hyperbola-keyring-$(V)/

upload:
	rsync -av -e 'ssh -p 51000' --progress hyperbola-keyring-$(V).tar.lz{,.sig} repo@dusseldorf.hyperbola.info:/srv/repo/sources/hyperbola-keyring

.PHONY: install uninstall dist upload
